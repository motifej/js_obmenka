window.onload = function () {
	var convertFrom = document.querySelector(".convert__sel_from");
	var convertTo = document.querySelector(".convert__sel_to");
	var resultInp = document.querySelector(".result-inp");
	var dataInp = document.querySelector(".data-inp");
	var currencyList = ["UAH"];
  var currency = {};
  var selectedOpt = '';
  var selectedOpt2 = '';
  var URL = 'https://api.privatbank.ua/p24api/pubinfo?json&exchange&coursid=5';

  (function loadData() {
  	var hrc = new XMLHttpRequest();
    hrc.open('GET', URL, true);
    hrc.onreadystatechange = function () {
        if (this.readyState !== this.DONE) {
            return;
        }

        if (this.status !== 200) {
            return;
        }
        parseData(this.response);
        drawOptions(convertFrom);
        showMainMenu();
    };
    hrc.send();
	})();

	function showMainMenu() {
		document.querySelector(".before-load").style.display = "none";
		document.querySelector(".main-section").style.display = "block";
	}

	function parseData (data) {
		var answ = JSON.parse(data);
    currency = answ.reduce((out,el) => {
    	if(el.ccy !== 'BTC') {
      	out[el.ccy]= el;
      	currencyList.push(el.ccy);
    	}
      	return out;
      },{"UAH": {buy: 1,sale:1}
    }); 
	}

	function drawOptions (el,nonList) {
		var list;
		var docfrag = document.createDocumentFragment();
		if (nonList)  {
			list = currencyList.filter(function(el){return el !== nonList});
		} else {
			list = currencyList;
		}
		list.forEach(function(e) {
  		var option = document.createElement("option");
  		option.textContent = e;
  		option.setAttribute('value',e);
  		docfrag.appendChild(option);
		});
		el.innerHTML='';
		el.appendChild(docfrag);
		el.value = '';
	}

	document.querySelector('.convert__sel_from').addEventListener('change',function(){
		selectedOpt = event.target.value;
		drawOptions(convertTo,selectedOpt);
		selectedOpt2 = '';
		setResult("");
	});

	document.querySelector('.convert__sel_to').addEventListener('change',function(){
		selectedOpt2 = event.target.value;
		setResult("");
	});

	document.querySelector(".btn").addEventListener('click',function(){
		event.preventDefault();
		if (!check()) {
			alert('Fill all fild');
			return;
		}
		var result = conv(selectedOpt,selectedOpt2).toFixed(5);
		setResult(result);
	});

	function conv(inpFrom,inpTo) {
		return dataInp.value * currency[inpFrom].sale / currency[inpTo].buy;
	}

	function setResult(res) {
		resultInp.value = res;
	}

	function check() {
		return selectedOpt2 && selectedOpt && dataInp.value;
	}

}