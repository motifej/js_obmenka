$( document ).ready(function($) {
	var convertFrom = $(".convert__sel_from");
	var convertTo = $(".convert__sel_to");
	var resultInp = $(".result-inp");
	var dataInp = $(".data-inp");
	var currencyList = ["UAH"];
	var currency = {};
	var selectedOpt = '';
	var selectedOpt2 = '';
	var URL = 'https://api.privatbank.ua/p24api/pubinfo?json&exchange&coursid=5';

	(function loadData() {
		$.get( URL, activateApp)
		.fail(errLoad);
	})();

	convertFrom.on('change',function(){
		selectedOpt = event.target.value;
		drawOptions(convertTo,selectedOpt);
		selectedOpt2 = '';
		setResult("");
	});

	convertTo.on('change',function(){
		selectedOpt2 = event.target.value;
		setResult("");
	});

	dataInp.on('mousedown', function() {
		setResult("");
	})

	$(".btn").on('click',function(){
		event.preventDefault();
		if (!check()) {
			alert('Fill all fild');
			return;
		}
		var result = conv(selectedOpt,selectedOpt2).toFixed(5);
		setResult(result);
	});

	function activateApp (data) {
		parseData(data);
		drawOptions(convertFrom);
		showMainMenu();
	}
	function errLoad() {
		alert( "Responce done with Error,Reload page!!!" );
	}
	function showMainMenu() {
		$(".before-load").hide();
		$(".main-section").show();
	}
	function parseData (data) {
		currency = data.reduce((out,el) => {
			if(el.ccy !== 'BTC') {
				out[el.ccy]= el;
				currencyList.push(el.ccy);
			}
			return out;
		},{"UAH": {buy: 1,sale:1}
	}); 
	}
	function drawOptions (el,nonList) {
		var list;
		var docfrag = document.createDocumentFragment();
		if (nonList)  {
			list = currencyList.filter(function(el){return el !== nonList});
		} else {
			list = currencyList;
		}
		list.forEach(function(e) {
			var option = $("<option></option>");
			option.val(e);
			option.text(e);
			docfrag.appendChild(option[0]);
		});
		el.empty();
		el.append(docfrag);
		el.val("");
	}
	function conv(inpFrom,inpTo) {
		return dataInp.val() * currency[inpFrom].sale / currency[inpTo].buy;
	}
	function setResult(res) {
		resultInp.val(res);
	}
	function check() {
		return selectedOpt2 && selectedOpt && dataInp.val();
	}

});